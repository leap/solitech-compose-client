#!/bin/bash

source ./.env

IMAGE=solitech-compose-client-bridge-client
SCRIPT_DIR=$(dirname "$0")
RESULTS_DIR="$SCRIPT_DIR/../results"
LOG_NAME=$(date '+%F_%H:%M:%S_utc_logs.txt')
START_TIME=$(date '+%FT%H:%M:%S')

GREEN='\033[0;32m'
RED='\033[0;31m'
ORANGE='\033[0;33m'
DIM='\033[2m'
NC='\033[0m'

shutdown_docker() {
    # write log files if tests have been executed
    if [[ -z $SKIP_TEST ]]; then
        if [[ ! -d $RESULTS_DIR ]]; then
            mkdir $RESULTS_DIR
        fi
        echo "" # ^C (Ctrl+c) is logged in first line
        docker compose logs --no-log-prefix --timestamps --since $START_TIME | tee $RESULTS_DIR/$LOG_NAME

        # Upload log to server
        response=$(curl -s -w "%{http_code}" -X POST \
            -H "X-API-Key: $API_KEY" \
            -F "log=@$RESULTS_DIR/$LOG_NAME" \
            "$LOG_SERVER_URL:$LOG_SERVER_PORT/logupload")

        # Extract the HTTP status code
        http_code=${response: -3}

        # Extract the response body (everything except the last line)
        body=$(echo "$response" | sed '$d')

        # Check the response
        if [ "$http_code" -eq 200 ]; then
            # Delete the log file from disk
            rm $RESULTS_DIR/$LOG_NAME
            printout """${GREEN}Файл журнала успешно загружен и удален с диска.
    ${DIM}Log file uploaded successfully and deleted from disk.${NC}
            """
        else
            # Automatic log file transfer didn't work. Keep log file and ask users to upload it.
            printout """${RED}
    Не удалось загрузить файл журнала. Код состояния: $http_code
    ${DIM}Failed to upload file. Status code: $http_code. Error: $body
            
    ${ORANGE}Лог записывается в results/$LOG_NAME
    Пожалуйста, загрузите файл журнала и удалите его после этого.
    ${DIM}Log file written to results/$LOG_NAME
    Please upload the log file and delete it afterwards.${NC}
            """
        fi
    fi
    if [[ $(docker ps | grep $IMAGE | grep "Up" | wc -c) -gt 0 ]]; then
        printout """Остановка docker …
    ${DIM}Stopping docker …${NC}"""
        docker compose stop
    fi
    if [[ -z $KEEP ]]; then
	    docker compose down --rmi all -v
    fi
}

printout() {
    echo -e """

    $1

    """
}

docker_compose_check() {
    DOCKER_VERSION_OUTPUT=$(docker compose version 2>/dev/null)

    if [[ -z $DOCKER_VERSION_OUTPUT ]]; then
        printout """
        ${RED}Похоже, что Docker compose отсутствует или его нет в вашей переменной PATH.
        Пожалуйста, прочитайте https://docs.docker.com/compose/install/, как установить docker compose.${NC}
        ${RED}${DIM}Docker compose seems to be missing or is not in your PATH variable.
        Please read https://docs.docker.com/compose/install/ how to install docker compose. ${NC}
        """
        exit
    fi
}

if [[ $1 == "build" ]]; then
    BUILD=true
    KEEP=true
elif [[ $1 == "mobile" ]]; then
    export MOBILE_NETWORK=true
elif [[ $1 == "clear" ]]; then
    CLEAR=true
elif [[ $1 == "--keep" ]]; then
    KEEP=true
elif [[ -n $1 ]]; then
    printout """Использование ${DIM}/ Usage${NC}:
    build-and-run.sh ------------------- Запустить тесты LAN/Wifi, при необходимости импортируйте докер-контейнер.
                                         ${DIM}Run LAN/Wifi tests, import docker container if required.${NC}
    build-and-run.sh build ------------- Cоздать докер-контейнер с нуля.
                                         ${DIM}Build docker container from scratch.${NC}
    build-and-run.sh mobile ------------ Запустить тесты мобильной сети, импортируйте докер-контейнер, если требуется.
                                         ${DIM}Run cellular network tests, import docker container if required.${NC}
    build-and-run.sh clear ------------- Удалить контейнер докера и его образ из системы.
                                         ${DIM}remove docker container and image from system.${NC}
    Используйте опцию --keep, чтобы сохранить созданный контейнер.
    ${DIM}Use the option --keep to keep the built container.${NC}
    """
    SKIP_TEST=true
    exit
fi

if [[ $2 == "--keep" ]]; then
    KEEP=true
fi

# --------------------------------------------- MAIN ----------------------------------------------
trap "exit" INT TERM
trap "shutdown_docker" EXIT

docker_compose_check

if [[ -n $CLEAR ]]; then
    docker compose down --rmi all -v
    SKIP_TEST=true
    exit
fi;

if [[ -z $BUILD ]]; then
    if [[ -f $SCRIPT_DIR/../exported/container.tar ]]; then
        # Import docker container
        printout """Импорт докер-контейнера exported/container.tar ...
    ${DIM}Importing docker container exported/container.tar ...${NC}"""
        cat $SCRIPT_DIR/../exported/container.tar | docker import - solitech-compose-client-bridge-client
        docker compose up -d bridge-client --remove-orphans
    elif [[ -n $(docker ps -a | grep $IMAGE | grep Exited) ]]; then
        # Reuse built container
        printout """Запуск существующего контейнера
    ${DIM}Starting existing container${NC}"""
        docker compose up -d bridge-client --remove-orphans
    else
        # Neither exported nor built container exists.
        printout """${RED}Не удалось импортировать контейнер Docker из файла exported/container.tar,
    а также не удалось найти существующий собранный контейнер. Либо запустите
        ./scripts/build-and-run.sh build,
    чтобы собрать контейнер из образа, либо выполните
        ./scripts/export.sh
    и экспортируйте собранный контейнер в новый файл.
    ${DIM}Could not import Docker container from exported/container.tar
    and also did not find an existing built container. Either run
        ./scripts/build-and-run.sh build
    to build the container from the image or
    build and export the built container to a new file by running
        ./scripts/export.sh${NC}"""
        SKIP_TEST=true
        exit
    fi
else
    # Build the docker container from scratch
    printout """Создание контейнера
    ${DIM}Creating container${NC}"""
    rm -f exported/container.tar
    docker compose build --no-cache --parallel
    docker compose up -d --build
    printout """
    Контейнеры построены. Продолжаем работу с
        ./scripts/build-and-run.sh mobile
    если вы находитесь в мобильной точке доступа (сотовая сеть) или
        ./scripts/build-and.run.sh
    если вы используете Wifi / LAN.
    ${DIM}Containers built. Continue with
        ./scripts/build-and-run.sh mobile
    if you're on a mobile hotspot (cellular network) or
        ./scripts/build-and.run.sh
    if you're using Wifi / LAN.${NC}
    """
    SKIP_TEST=true
    exit
fi

printout """Ожидание окончания тестирования. Это может занять до нескольких минут …
    ${DIM}Waiting until tests finish. This can take up to several minutes …${NC}"""
until [[ $(docker ps | grep $IMAGE | grep "Up" | wc -c) -eq 0 || \
            $(docker compose logs --since $START_TIME | grep "STARTING SQUID" | wc -c) -gt 0 ]]; do
    sleep 5;
done

if [[ $(docker ps | grep $IMAGE | grep "Up" | wc -c) -eq 0 ]]; then
    printout """${RED}Ручные тесты пропущены.${NC}
    ${RED}${DIM}Manual Tests skipped.${NC}"""
    exit
fi

echo -e """    Автоматические тесты завершены.
    ${DIM}Automated tests finished.${NC}
    

    ${GREEN}Теперь вы можете установить в настройках http-прокси в вашем браузере хост localhost и порт 3129 (localhost:3129).
    Также убедитесь, что вы активировали DNS of HTTPS (DoH) в настройках вашего браузера.${NC}
    ${GREEN}${DIM}You can now set the http proxy settings in your browser to host localhost and port 3129 (localhost:3129).
    Please make also sure that you activate DNS of HTTPS (DoH) in the settings of your browser.${NC}

    ${ORANGE}Пожалуйста, проверьте свой IP-адрес и предполагаемое местоположение в браузере, посетив сайт https://wtfismyip.com.${NC}
    ${ORANGE}${DIM}Please double check your IP and estimated location in your browser by visiting https://wtfismyip.com. ${NC}

    Нажмите Ctrl+c, чтобы выключить HTTP-прокси.
    ${DIM}Press Ctrl+c to shutdown the HTTP proxy${NC}


"""

until [[ $(docker ps | grep $IMAGE | grep "Up" | wc -c) -eq 0 ]]; do
    sleep 5;
done;

