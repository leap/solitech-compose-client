#!/bin/bash

IMAGE=solitech-compose-client-bridge-client
SCRIPT_DIR=$(dirname "$0")
EXPORT_DIR=$SCRIPT_DIR/../exported


if [[ $1 == "no-build" ]]; then
    NOBUILD=true
fi

if [[ -n $NOBUILD ]]; then
    if [[ -z $(docker ps -a | grep $IMAGE) ]]; then
        $SCRIPT_DIR/build-and-run.sh build
    fi
else
    cd $SCRIPT_DIR/..
    docker compose down --rmi all -v
    ./scripts/build-and-run.sh build
    cd -
fi

if [[ ! -d $EXPORT_DIR ]]; then
    mkdir $EXPORT_DIR
fi

DOCKER_CONTAINER=$(docker ps -a | grep $IMAGE | grep Exited | rev | cut -d " " -f1 | rev) 
docker export $DOCKER_CONTAINER > $EXPORT_DIR/container.tar
ls -lah $EXPORT_DIR/container.tar
