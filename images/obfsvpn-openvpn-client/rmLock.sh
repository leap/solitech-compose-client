#!/bin/bash

set -e

echo "openvpn up, removing lockfile"
rm /tmp/LOCKFILE || echo "removal failed"