FROM golang:1.22.3 AS builder

ENV SOURCE_PATH ${GOPATH}/src/0xacab.org/leap/obfsvpn
COPY . ${SOURCE_PATH}/
WORKDIR ${SOURCE_PATH}
ENV OBFSVPN_COMMIT e6b06efc4456581b3dc966c98bd558462d516a4e
RUN git clone https://0xacab.org/leap/obfsvpn/ && cd obfsvpn && git reset --hard $OBFSVPN_COMMIT
RUN cd obfsvpn && go get ./... && CGO_ENABLED=0 go build -o obfsvpn-client ./cmd/client/ && cp obfsvpn-client /obfsvpn-client

ENV NDT7_COMMIT v0.8.0
RUN git clone https://github.com/m-lab/ndt7-client-go.git && cd ndt7-client-go && git reset --hard $NDT7_COMMIT 
RUN cd ndt7-client-go && CGO_ENABLED=0 go build -o ndt7-client ./cmd/ndt7-client && cp ndt7-client /ndt7-client


FROM alpine:3.21.0 as alpine

# copy obfsvpn-client from builder
COPY --from=builder /obfsvpn-client /usr/bin/
# copy ndt7 client
COPY --from=builder /ndt7-client /usr/bin/
# Install openvpn
RUN apk --no-cache --no-progress upgrade && \
    apk --no-cache --no-progress add bash coreutils curl ip6tables iptables openvpn bind-tools iperf3 \
                shadow dumb-init tzdata inotify-tools jq whois squid && \
    addgroup -S vpn && \
    rm -rf /tmp/*


FROM alpine

COPY images/obfsvpn-openvpn-client/start.sh /usr/bin/
COPY images/obfsvpn-openvpn-client/rmLock.sh /usr/bin/
COPY images/obfsvpn-openvpn-client/dnsleaktest.sh /usr/bin/

VOLUME ["/vpn"]

ENTRYPOINT ["dumb-init"]
