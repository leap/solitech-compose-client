#!/bin/bash

set -eo pipefail

mkdir -p /dev/net

kill_squid() {
    # Only kill squid if it is actually running
    if pgrep -x "squid" > /dev/null; then
        # Squid is running
        echo "Killing squid"
        squid -k kill
    fi
}

kill_foreground() {
    echo "A background task has finished. Killing other backround tasks before exiting."
    kill_squid
    kill 0
    exit 1
}

kill_background() {
    if [[ -f /tmp/LOCKFILE ]]; then
        echo "clean up LOCK"
        rm /tmp/LOCKFILE
    fi
    kill_squid
    echo "Foreground task finished."
    kill 0;
}

# Function to check internet connection
check_internet() {
    local timeout=5
    if ping -c 1 -W $timeout 8.8.8.8 &> /dev/null; then
        return 0
    else
        return 1
    fi
}

# Run a ndt test with the option to give a custom server
run_network_test() {
    local server="$1"
    if [ -n "$server" ]; then
        ndt7-client -server "$server" -anonymize.ip netblock -format json -quiet 2> /dev/null | jq '.ClientIP = "an.on.y.mi.ze.d"' 2>/dev/null || return 1
    else
        ndt7-client -anonymize.ip netblock -format json -quiet 2> /dev/null | jq '.ClientIP = "an.on.y.mi.ze.d"' 2>/dev/null || return 1
    fi
}

if ! [ -c /dev/net/tun ]; then
    echo "Creating tun/tap device."
    mknod /dev/net/tun c 10 200
fi

trap "exit" INT TERM
trap "kill_background" EXIT

# Check for internet connection
if ! check_internet; then
    echo "Error: No internet connection detected. Please check your network settings." >&2
    exit 1
fi

# Network info retrieval
NETWORKINFO=$(curl https://wtfismyip.com/json 2> /dev/null)
IP=$(echo $NETWORKINFO | jq -r ".YourFuckingIPAddress")
CITY=$(echo $NETWORKINFO | jq -r ".YourFuckingCity")
ISP=$(echo $NETWORKINFO | jq -r ".YourFuckingISP")
ASN=$(whois $IP | grep -i origin | sed s/origin\://g | awk '{$1=$1};1')
echo "Date: $(date)"
echo "ASN: $ASN"
echo "ISP: $ISP"
echo "estimated City: $CITY"
echo "Mobile network: $MOBILE_NETWORK"
echo "USER: $CLIENT_USER"
echo "REGION: $REGION"
echo "CLIENT VERSION: $SOLITECH_COMPOSE_CLIENT_COMMIT"

# Baseline testing
echo "PING BASELINE:"
ping -c 10 8.8.8.8
echo "NDT BASELINE:"
if output=$(run_network_test "ndt.vikagusieva.site") || output=$(run_network_test); then
    echo "$output"
else
    echo "Both baseline network tests failed" >&2
    exit 1
fi

FLAG_KCP=""
if [[ "$KCP" == "1" ]]; then
    FLAG_KCP="-kcp"
    echo "Using KCP on the wire"
elif [[ "$QUIC" == "1" ]]; then
    echo "Using QUIC on the wire"
    FLAG_QUIC="-quic"
fi

if [[ "$HOP_PT" == "1" ]]; then
    if [[ "$KCP" == "1" ]]; then
        echo "Starting obfsvpn-client in hopping mode (obfs4: kcp, openvpn: udp)"
    elif [[ "$QUIC" == "1" ]]; then
        echo "Starting obfsvpn-client in hopping mode (quic, openvpn: udp)"
    else
        echo "Starting obfsvpn-client in hopping mode (obfs4: tcp, openvpn: udp)"
    fi;

    # set a couple of optional hopping flags
    FLAG_MIN_HOP_PORT=""
    if [[ -n $OBFSVPN_MIN_HOP_PORT ]]; then
        FLAG_MIN_HOP_PORT="-min-port $OBFSVPN_MIN_HOP_PORT"
        echo "Setting Min hopping port to $OBFSVPN_MIN_HOP_PORT"
    fi

    FLAG_MAX_HOP_PORT=""
    if [[ -n $OBFSVPN_MAX_HOP_PORT ]]; then
        FLAG_MAX_HOP_PORT="-max-port $OBFSVPN_MAX_HOP_PORT"
        echo "Setting max hopping port to $OBFSVPN_MAX_HOP_PORT"
    fi

    FLAG_SEED=""
    if [[ -n $OBFSVPN_SEED ]]; then
        FLAG_SEED="-ps $OBFSVPN_SEED"
        echo "Setting hopping port seed to $OBFSVPN_SEED"
    fi

    FLAG_PORT_COUNT=""
    if [[ -n $OBFSVPN_PORT_COUNT ]]; then
        FLAG_PORT_COUNT="-pc $OBFSVPN_PORT_COUNT"
        echo "Setting hopping port count to $OBFSVPN_PORT_COUNT"
    fi

    /usr/bin/obfsvpn-client -h $FLAG_KCP $FLAG_QUIC -c "$OBFS4_CERT1,$OBFS4_CERT2" -r "$OBFS4_SERVER_HOST1,$OBFS4_SERVER_HOST2" \
      -m "$MIN_HOP_SECONDS" -j "$HOP_JITTER" $FLAG_MAX_HOP_PORT $FLAG_MIN_HOP_PORT $FLAG_SEED $FLAG_PORT_COUNT -v &
else
    if [[ "$KCP" == "1" ]]; then
        echo "Starting obfsvpn-client in KCP mode (obfs4: kcp, openvpn: udp)"
    elif [[ "$QUIC" == "1" ]]; then
        echo "Starting obfsvpn-client in QUIC mode (quic, openvpn: udp)"
    else
        echo "start the obfsvpn-client in obfs4 mode (obfs4: tcp, openvpn: tcp)"
    fi;
    /usr/bin/obfsvpn-client $FLAG_KCP $FLAG_QUIC -c ${OBFS4_CERT1} -r ${OBFS4_SERVER_HOST1} -rp ${OBFS4_PORT} -v &
fi;

# observe obfsvpn-client process and kill foreground process in case obfsvpn-client dies
export PID1=$!
( tail -f --pid=$PID1 /dev/null; echo "obfsvpn died :("; kill_foreground ) &

# create a lock file to wait until openvpn tunnel was setup
LOCK=/tmp/LOCKFILE
touch $LOCK

if [[ "$HOP_PT" == "1" ]]; then
    # start openvpn in udp hopping mode
    openvpn --config /vpn/client.ovpn \
            --remote 127.0.0.1 8080 \
            --route ${OBFS4_SERVER_HOST1} 255.255.255.255 net_gateway \
            --route ${OBFS4_SERVER_HOST2} 255.255.255.255 net_gateway \
            --proto udp \
            --script-security 2 --up /usr/bin/rmLock.sh \
            --suppress-timestamps \
            --verb $OPENVPN_VERB &
elif [[ "$KCP" == "1" ]] || [[ "$QUIC" == "1" ]]; then
    # start openvpn in udp mode
    openvpn --config /vpn/client.ovpn \
            --remote 127.0.0.1 8080 \
            --route ${OBFS4_SERVER_HOST1} 255.255.255.255 net_gateway \
            --proto udp \
            --script-security 2 --up /usr/bin/rmLock.sh \
            --suppress-timestamps \
            --verb $OPENVPN_VERB &
else
    # start openvpn in tcp mode (since we use --socks-proxy)
    openvpn --config /vpn/client.ovpn \
            --socks-proxy 127.0.0.1 8080 \
            --remote ${OBFS4_SERVER_HOST1} ${OBFS4_PORT} \
            --route ${OBFS4_SERVER_HOST1} 255.255.255.255 net_gateway \
            --proto tcp \
            --script-security 2 --up /usr/bin/rmLock.sh \
            --suppress-timestamps \
            --verb $OPENVPN_VERB &
fi


# observe openvpn client process and kill foreground process in case openvpn client dies
export PID2=$!
( tail -f --pid=$PID2 /dev/null; echo "openvpn died :("; kill_foreground ) &

# Watch lockfile and block until deleted
inotifywait -e delete_self $LOCK

# perform tests
echo "PING tun0:"
ping -c 10 -I tun0 8.8.8.8


echo DNS leak test tun0:
dnsleaktest.sh tun0 || echo "Error: failed to run dns leak test for tun0"
echo DNS leak test eth0:
dnsleaktest.sh eth0 || echo "Error: failed to run dns leak test for eth0"

echo "NDT tun0:"
if output=$(run_network_test "ndt.vikagusieva.site") || output=$(run_network_test); then
    echo "$output"
else
    echo "Both tunnel network tests failed." >&2
    exit 1
fi

echo "STARTING SQUID"
if [[ -z $(ps | grep squid | grep -v grep) ]]; then
    squid
fi
sleep infinity
exit $?