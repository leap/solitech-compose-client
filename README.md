# Network Testing Docker client

A simple Docker client to test network conditions.

## Environment variables

The following environment variables are required to start the build script.

| Variable |  <div style="width:250px">property</div>Purpose | Example |
|:--------:|:--------------:|:--------:|
|OBFS4_SERVER_HOST1|Bridge server host|123.123.123.123|
|OBFS4_PORT|Bridge port the client is trying to connect to| 4430 |
|OPENVPN_HOST|Gateway server host the bridge is pointing to|231.231.231.231|
|OPENVPN_PORT|Gateway server port the bridge is pointing to|443|
|HOP_PT| use hopping PT mode| 1 for `true` |
|KCP| use KCP mode| 1 for `true` |
|OFBS4_CERT1|obfs4 certificate of the bridge| 70-digit string|
|CLIENT_USER| an optional user id |U1|
|REGION| an optional region, cross check for geoip lookup |Saxony|
|LOG_SERVER_URL| the URL of the server accepting the resulting log file| https://logserver.org|
|LOG_SERVER_PORT| the port number the log server exposes to accept log files| 8778|

## Build and run

1. Make sure you have set all the environment variables above. You can create an `.env` file at the root directory of this repository.
2. You'll also need the following files:
- `data/ca.pem`
- `data/vpn.crt`
- `data/vpn.key`
These need to be specific to the provider you're testing against.

3. Run `./scripts/build-and-run.sh` to run the tests on Wifi/LAN. Pass the parameter `mobile` if you start the test on a mobile network.
This will try to import a docker container from exported/container.tar. If there is no such file, the script will look for a built container from the list (`docker ps -a`). If there is none, you need to build one first using the build option or by running the export script

By default the built container will be removed at the end of the tests. If you want to keep it use the --keep flag, e.g.
`./scripts/build-and-run.sh --keep`

## Rebuild the container (without exporting it)
`./scripts/build-and-run.sh build`

## Remove containers and images
`./scripts/build-and-run.sh clear`

## Export container
A container can be created and exported with:
`./scripts/export.sh`

## Update dependencies
In case of an obfsvpn or NDT client version update replace there the referenced commits in the [Dockerfile](./images/obfsvpn-openvpn-client/Dockerfile). You also may need to adapt the environment variables in your `.env` file.
